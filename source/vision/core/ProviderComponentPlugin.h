/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       18.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vision/core/CameraServer.h>
#include <ArmarXCore/core/ComponentPlugin.h>


#define EXPLICIT_PC_PROVIDE_INSTANTIATION_DECLARATION(r, data, elem) \
extern template                                                      \
void ProviderComponentPlugin::provide<vision::core::traits::PointCloud<ENUM_TYPE(elem)>::point_type>(const ENUM_TYPE(elem)&, \
const std::string&, const std::string&);

namespace armarx::vision::core
{

    class ProviderComponentPlugin : public ComponentPlugin
    {
    public:
        ProviderComponentPlugin(ManagedIceObject& parent, const std::string& prefix) :
                ComponentPlugin(parent, prefix)
        {
        };

        void provide(const cv::Mat& image, const std::string& providerName, const std::string& instanceName,
                     const std::optional<armarx::DateTime>& provisionTimestamp);

        template <typename PointT>
        void provide(const pcl::PointCloud<PointT>& pointCloud, const std::string& providerName,
                     const std::string& instanceName);

        void addProvider(const std::string& providerName, const arondto::ProviderMetaInfo& info);
        void addVisualData(const std::string& providerName, const std::string& instanceName,
                           const arondto::DataInstanceDescription type);

    protected:
        template <typename DataT>
        void provide(const DataT& visualData,
                     const arondto::DataInstanceMetaInfo& info, const std::string& providerName,
                     const std::string& instanceName);

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

        void componentPropertiesUpdated(const std::set<std::string>& changedProperties) override;

        void preOnInitComponent() override;

        void postOnInitComponent() override;

        void preOnConnectComponent() override;

        void postOnConnectComponent() override;

        void preOnDisconnectComponent() override;

        void postOnDisconnectComponent() override;

        void preOnExitComponent() override;

        void postOnExitComponent() override;

    private:

        std::map<std::string, std::pair<IceInternal::Handle<CameraServer>,
                                        arondto::ProviderMetaInfo>> blobProviderMap;
    };

    FOR_EACH_POINTCLOUD(EXPLICIT_PC_PROVIDE_INSTANTIATION_DECLARATION);
}

#undef EXPLICIT_PC_PROVIDE_INSTANTIATION_DECLARATION