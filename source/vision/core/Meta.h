/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       18.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <type_traits>
#include <vision/core/ForwardDeclarations.h>




namespace armarx::vision::core
{
    using Image = cv::Mat;
    template <typename PointT>
    using PointCloud = pcl::PointCloud<PointT>;

    template <typename>
    struct is_pointcloud : public std::false_type
    {
    };

    template <typename T>
    struct is_pointcloud<PointCloud<T>> : public std::true_type
    {
    };

    template <typename T> inline constexpr bool is_pointcloud_v = is_pointcloud<T>::value;

    template <typename T> constexpr bool is_visualData_v = std::is_same_v<T, Image> || is_pointcloud_v<T>;

    template <typename T> concept isVisualData = is_visualData_v<T>;

    template <class To, class From>
    std::enable_if_t<
            sizeof(To) == sizeof(From) &&
            std::is_trivially_copyable_v<From> &&
            std::is_trivially_copyable_v<To>,
            To>
    // constexpr support needs compiler magic
    bit_cast(const From& src) noexcept
    {
        static_assert(std::is_trivially_constructible_v<To>,
                      "This implementation additionally requires "
                      "destination type to be trivially constructible");

        To dst;
        std::memcpy(&dst, &src, sizeof(To));
        return dst;
    }

    template <typename DataT>
    using DataWithMetaInfo = std::pair<DataT, arondto::DataInstanceMetaInfo>;
}