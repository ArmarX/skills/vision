/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       16.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/time/ice_conversions.h>
#include <opencv2/core/mat.hpp>
#include <vision/core/communication/Encoding.h>

#include "CameraServer.h"

#define EXPLICIT_PC_PROVIDE_INSTANTIATION(r, data, elem) \
template                                                      \
void CameraServer::update<elem>(const std::string&, const elem&, \
const arondto::DataInstanceMetaInfo&);

namespace armarx::vision::core
{
    CameraServer::CameraServer(const std::string& providerName, const arondto::ProviderMetaInfo& metaInfo) :
            numberOfIceConsumers(0),
            numberOfSharedMemoryConsumers(0), dataManager(std::make_unique<VisualDataDataManager>(providerName, metaInfo)), memoryName(providerName + ".IceBlobProvider")
    {
    }

    template <typename DataT>
    void CameraServer::update(const std::string& instanceName, const DataT& visualData,
                              const arondto::DataInstanceMetaInfo& info)
    {
        ARMARX_TRACE;
        if (numberOfSharedMemoryConsumers)
        {
            ARMARX_TRACE;
            {
                auto& buffer = dataManager->getSharedMemoryBuffer(instanceName);
                communication::encode(buffer, visualData, info);
            }
            dataManager->notifySharedMemoryUpdated(instanceName);
        }
        if (numberOfIceConsumers)
        {
            ARMARX_TRACE;
            std::unique_lock lock(newerThanMutex);
            BlobWithMetaInfo blob;
            communication::encode(blob, visualData, info);
            dataManager->getIceBuffer(instanceName) = BlobWithMetaInfo::toIce(blob);
            newerThanCondition.notify_all();
        }
    }

    void CameraServer::addSharedMemoryConsumer(const Ice::Current& current)
    {
        ARMARX_TRACE;
        numberOfSharedMemoryConsumers++;
    }

    void CameraServer::removeSharedMemoryConsumer(const Ice::Current& current)
    {
        ARMARX_TRACE;
        numberOfSharedMemoryConsumers--;
    }

    void CameraServer::addIceConsumer(const Ice::Current& current)
    {
        ARMARX_TRACE;
        numberOfIceConsumers++;
    }

    void CameraServer::removeIceConsumer(const Ice::Current& current)
    {
        ARMARX_TRACE;
        numberOfIceConsumers--;
    }

    void CameraServer::addVisualData(const std::string& instanceName, const arondto::DataInstanceDescription& type)
    {
        ARMARX_TRACE;
        dataManager->addVisualData(instanceName, type);
    }

    void CameraServer::getBlobNewerThan_async(const itfc::AMD_BlobProviderInterface_getBlobNewerThanPtr& ptr,
                                              const std::string& instanceName,
                                              const armarx::core::time::dto::DateTime& dateTime, ::Ice::Long waitForMS,
                                              const Ice::Current& current)
    {
        ARMARX_TRACE;
        checkAndRemoveAMDFutures();
        amdFutures.emplace_back(std::async(std::launch::async, [this, ptr, waitForMS, dateTime, instanceName]() -> void
        {
            ARMARX_TRACE;
            armarx::core::time::DateTime time;
            fromIce(dateTime, time);
            std::unique_lock lk(newerThanMutex);
            newerThanCondition.wait_for(lk, std::chrono::milliseconds(waitForMS), [this, &time, &instanceName]() -> bool
            {
                ARMARX_TRACE;
                bool isNewerThan{true};
                auto buffer = dataManager->getIceBuffer(instanceName);
                arondto::DataInstanceMetaInfo info;
                if (not buffer.metaInfo) return false;
                info.fromAron(buffer.metaInfo);
                isNewerThan &= info.provisionTimestamp >= time;
                return isNewerThan;
            });
            // TODO: Fix race condition for copying of ice buffers
            ptr->ice_response(dataManager->getIceBuffer(instanceName));
        }));
    }

    void CameraServer::checkAndRemoveAMDFutures()
    {
        amdFutures.erase(std::remove_if(amdFutures.begin(), amdFutures.end(), [](auto& future) -> bool
        {
            ARMARX_TRACE;
            if (!future.valid())
            {
                ARMARX_TRACE;
                return true;
            }
            std::future_status status = future.wait_for(std::chrono::seconds(0));
            if (status == std::future_status::ready)
            {
                ARMARX_TRACE;
                future.get();
                return true;
            } else if (status == std::future_status::deferred)
            {
                ARMARX_TRACE;
                // if the task has not been started --> start the task
                throw armarx::RuntimeError("Futures should not be in this state!");
            } else
            {
                ARMARX_TRACE;
                return false;
            }
        }), amdFutures.end());
    }

    std::string CameraServer::getDefaultName() const
    {
        ARMARX_TRACE;
        return memoryName;
    }

    void CameraServer::onInitComponent()
    {
        ARMARX_TRACE;
    }

    void CameraServer::onConnectComponent()
    {
        ARMARX_TRACE;
    }

    void CameraServer::getBlobImmediately_async(const itfc::AMD_BlobProviderInterface_getBlobImmediatelyPtr& ptr,
                                                const std::string& instanceName, const Ice::Current& current)
    {
        ARMARX_TRACE;
        checkAndRemoveAMDFutures();
        // TODO: Fix race condition for copying of ice buffers
        amdFutures.emplace_back(std::async(std::launch::async, [this, ptr, instanceName]() -> void
        {
            ARMARX_TRACE;
            ARMARX_CHECK(dataManager->getLatestIceBuffers().find(instanceName) != dataManager->getLatestIceBuffers().end());
            ptr->ice_response(dataManager->getLatestIceBuffers().at(instanceName));
        }));
    }

    FOR_EACH_TYPE(EXPLICIT_PC_PROVIDE_INSTANTIATION);
}