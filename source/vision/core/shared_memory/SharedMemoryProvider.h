/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       22.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <experimental/memory>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <vision/core/ForwardDeclarations.h>

namespace armarx::vision::core::shared_memory
{
    using namespace boost::interprocess;
    struct SharedMemoryData;

    class SharedMemoryProvider
    {
        class ScopedLock;

    public:
        using ShmAllocatorT = allocator<std::pair<const std::size_t, SharedMemoryData>,
                                        managed_shared_memory::segment_manager>;
        using ShmDataT = map<std::size_t, SharedMemoryData, std::less<>, ShmAllocatorT>;

        SharedMemoryProvider(const std::string& newMemoryName, std::size_t capacity);


        /**
         * Destructs the shared memory provider. Removes the shared memory object of not used by another process.
         */
        ~SharedMemoryProvider();

        /**
         * Retrieve pointer to shared memory. Use getScopedWriteLock() or lock() / unlock() before writing to the memory.
         *
         * @return pointer to the shared memory with type MemoryObjectT
         */
        ShmDataT& getMemoryBuffer();

        void notifyUpdated(const std::string& dataInstanceName);

        /**
         * Retrieve size of usable shared memory.
         *
         * @return size in bytes
         */
        [[nodiscard]] std::size_t getCapacity() const;

        void addVisualData(const std::string& instanceName);

    private:
        std::string memoryName;
        std::unique_ptr<managed_shared_memory> sharedMemorySegment;
        std::experimental::observer_ptr<ShmDataT> memoryObject{nullptr};
        std::size_t shmCapacity;

    };


}

