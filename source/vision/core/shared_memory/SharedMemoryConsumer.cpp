/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       24.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SharedMemoryConsumer.h"

#include <ArmarXCore/core/services/sharedmemory/exceptions/SharedMemoryExceptions.h>
#include <vision/core/shared_memory/SharedMemoryQueryHelper.h>

namespace armarx::vision::core::shared_memory
{

    SharedMemoryConsumer::SharedMemoryConsumer(const std::string& newMemoryName) : memoryName(newMemoryName)
    {

        // create shared memory segment
        try
        {
            sharedMemorySegment = std::make_unique<managed_shared_memory>(open_only, memoryName.c_str());
        } catch (const interprocess_exception& e)
        {
            std::string reason = "Error opening shared memory segment for reading: ";
            throw exceptions::local::SharedMemoryConnectionFailure(memoryName, reason + e.what());
        }

        // create the managed image memory object
        memoryObject = std::experimental::make_observer(sharedMemorySegment->find<ShmDataT>("Data").first);

        if (!memoryObject)
        {
            throw exceptions::local::SharedMemoryConnectionFailure(memoryName, "Error opening shared data for reading");
        }
    }

    SharedMemoryQueryHelper SharedMemoryConsumer::getMemoryBuffer(const std::string& instanceName) const
    {
        return {memoryObject.get(), instanceName};
    }
}