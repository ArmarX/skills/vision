/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       22.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <experimental/memory>
// boost
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <boost/interprocess/containers/map.hpp>

namespace armarx::core::time
{
    class DateTime;

    class Duration;
}

namespace armarx::vision::core::shared_memory
{
    using namespace boost::interprocess;
    struct SharedMemoryData;

    class SharedMemoryQueryHelper;

    class SharedMemoryConsumer
    {
    public:
        using ShmAllocatorT = allocator<std::pair<const std::size_t, SharedMemoryData>,
                                        managed_shared_memory::segment_manager>;
        using ShmDataT = map<std::size_t, SharedMemoryData, std::less<>, ShmAllocatorT>;

        /**
         * Creates a shared memory consumer.
         *
         * @param memoryName name of the memory as set in the SharedMemoryProvider
         *
         * @throw SharedMemoryConnectionFailure
         */
        explicit SharedMemoryConsumer(const std::string& newMemoryName);

        /**
         * Destructs the shared memory consumer.
         */
        ~SharedMemoryConsumer() = default;

        /**
         * Retrieve size of usable shared memory.
         *
         * @return size in bytes
         */

        /**
         * Retrieve pointer to shared memory. Use getScopedReadLock() or lock() / unlock() before reading from the memory.
         *
         * @return pointer to the shared memory with type MemoryObject
         */
        [[nodiscard]] SharedMemoryQueryHelper getMemoryBuffer(const std::string& instanceName) const;

    private:
        std::string memoryName;
        std::unique_ptr<managed_shared_memory> sharedMemorySegment{nullptr};
        std::experimental::observer_ptr<ShmDataT> memoryObject{nullptr};
    };
}

