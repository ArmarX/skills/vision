/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       24.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/sync/interprocess_upgradable_mutex.hpp>
#include <vision/core/aron/ProviderMetaInfo.aron.generated.h>


namespace armarx::vision::core::shared_memory
{
    using namespace boost::interprocess;

    struct SharedMemoryData
    {
        using AllocatorT = boost::interprocess::allocator<std::byte, managed_shared_memory::segment_manager>;
        using VectorT = std::vector<std::byte, AllocatorT>;

        explicit SharedMemoryData(const AllocatorT& vector_allocator) :
                updateTimestampInMicroS(0),
                info(vector_allocator),
                data(vector_allocator)
        {
        };
        interprocess_condition condition;
        interprocess_mutex conditionMutex;
        interprocess_upgradable_mutex readWriteMutex;
        long updateTimestampInMicroS;
        VectorT info;
        VectorT data;
    };
}