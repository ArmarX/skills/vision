/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       11.10.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "SharedMemoryData.h"
#include "SharedMemoryConsumer.h"

namespace armarx::vision::core::shared_memory
{
    class SharedMemoryQueryHelper
    {
        using ShmDataT = SharedMemoryConsumer::ShmDataT;
    public:
        SharedMemoryQueryHelper(ShmDataT* sharedMemoryData, const std::string& instanceName);
        const SharedMemoryData& newerThan(const armarx::DateTime& timestamp, const armarx::Duration& timeout) const;
        const SharedMemoryData& immediately() const;
    private:
        void waitForUpdate(const armarx::DateTime& timestamp, const armarx::Duration& timeout) const;
        const SharedMemoryData& getBuffer() const;
        std::experimental::observer_ptr<SharedMemoryData> sharedMemoryBuffer;
    };
}