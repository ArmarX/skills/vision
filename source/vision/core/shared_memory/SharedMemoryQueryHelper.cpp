/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       11.10.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/time.h>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include "SharedMemoryQueryHelper.h"

namespace armarx::vision::core::shared_memory
{

    SharedMemoryQueryHelper::SharedMemoryQueryHelper(ShmDataT* sharedMemoryData,
                                                     const std::string& instanceName)
    {
        std::hash<std::string> hash;
        ARMARX_CHECK(sharedMemoryData->find(hash(instanceName)) != sharedMemoryData->end());
        sharedMemoryBuffer = std::experimental::make_observer(&sharedMemoryData->at(hash(instanceName)));
    }

    const SharedMemoryData& SharedMemoryQueryHelper::newerThan(const DateTime& timestamp, const Duration& timeout) const
    {
        waitForUpdate(timestamp, timeout);
        return getBuffer();
    }

    void SharedMemoryQueryHelper::waitForUpdate(const DateTime& timestamp, const Duration& timeout) const
    {
        scoped_lock<interprocess_mutex> lock{sharedMemoryBuffer->conditionMutex};
        boost::posix_time::ptime abs_time =
                microsec_clock::local_time() + boost::posix_time::microseconds(timeout.toMicroSeconds());
        sharedMemoryBuffer->condition.timed_wait(lock, abs_time, [this, &timestamp]() -> bool
        {
            sharable_lock<interprocess_upgradable_mutex> lock(sharedMemoryBuffer->readWriteMutex);
            armarx::core::time::DateTime updateTimestamp = armarx::core::time::Duration::MicroSeconds(
                    sharedMemoryBuffer->updateTimestampInMicroS);
            return updateTimestamp >= timestamp;
        });

    }

    const SharedMemoryData& SharedMemoryQueryHelper::immediately() const
    {
        return getBuffer();
    }

    const SharedMemoryData& SharedMemoryQueryHelper::getBuffer() const
    {
        sharable_lock<interprocess_upgradable_mutex> lock(sharedMemoryBuffer->readWriteMutex);
        return *sharedMemoryBuffer;
    }
}