/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       24.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SharedMemoryProvider.h"
#include <vision/core/shared_memory/SharedMemoryData.h>
#include <ArmarXCore/core/services/sharedmemory/exceptions/SharedMemoryExceptions.h>
#include <ArmarXCore/core/logging/Logging.h>

namespace armarx::vision::core::shared_memory
{
    void SharedMemoryProvider::notifyUpdated(const std::string& dataInstanceName)
    {
        std::hash<std::string> hash;
        auto it = memoryObject->find(hash(dataInstanceName));
        ARMARX_CHECK(it != memoryObject->end());
        auto& dataInstance = memoryObject->at(hash(dataInstanceName));
        {
            scoped_lock<interprocess_mutex> lock(dataInstance.conditionMutex);
            dataInstance.condition.notify_all();
        }

    }

    SharedMemoryProvider::SharedMemoryProvider(const std::string& newMemoryName, std::size_t capacity) :
            memoryName(newMemoryName),
            shmCapacity(capacity)
    {
        // sanity check
        if (capacity <= 0)
        {
            throw exceptions::local::SharedMemoryException(newMemoryName,
                                                           "Invalid memory capacity. Capacity must be >0");
        }

        // remove memory segment.
        // Remove will fail if memory is still in use by other process.
        // Consequently memory is only removed when no process has access to the memory
        shared_memory_object::remove(memoryName.c_str());

        // shared memory size = required + 1024 Bytes buffer for
        // boost allocation algorithm

        shmCapacity = capacity + sizeof(ShmDataT) + 1024;
        ARMARX_INFO << "Creating Shared Memory with size " << capacity << " Bytes";
        try
        {
            sharedMemorySegment = std::make_unique<managed_shared_memory>(open_or_create, memoryName.c_str(),
                                                                          shmCapacity);
        } catch (std::exception& e)
        {
            std::stringstream message;
            message << "Error creating shared memory segment. Still a consumer running?\nReason: " << e.what();
            throw exceptions::local::SharedMemoryException(memoryName, message.str());
        }

        //Initialize shared memory STL-compatible allocator
        const ShmAllocatorT alloc_inst(sharedMemorySegment->get_segment_manager());

        // create the managed memory object
        sharedMemorySegment->destroy<ShmDataT>("Data");
        memoryObject = std::experimental::make_observer(
                sharedMemorySegment->find_or_construct<ShmDataT>("Data")(alloc_inst));

        if (!memoryObject)
        {
            throw exceptions::local::SharedMemoryException(memoryName,
                                                           "Error constructing data in shared memory segment.");
        }


        ARMARX_INFO_S << "Created memory with name " << memoryName;
    }

    SharedMemoryProvider::~SharedMemoryProvider()
    {
        // remove memory segment
        shared_memory_object::remove(memoryName.c_str());
    }

    SharedMemoryProvider::ShmDataT& SharedMemoryProvider::getMemoryBuffer()
    {
        return *memoryObject;
    }

    std::size_t SharedMemoryProvider::getCapacity() const
    {
        return shmCapacity;
    }

    void SharedMemoryProvider::addVisualData(const std::string& instanceName)
    {
        ARMARX_CHECK_NOT_NULL(memoryObject);
        const ShmAllocatorT alloc_inst(sharedMemorySegment->get_segment_manager());
        std::hash<std::string> hash;
        memoryObject->emplace(hash(instanceName), alloc_inst);
    }

}