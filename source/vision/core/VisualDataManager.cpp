/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       04.10.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <vision/core/shared_memory/SharedMemoryProvider.h>
#include <vision/core/shared_memory/SharedMemoryData.h>
#include <vision/core/BlobWithMetaInfo.h>

#include "VisualDataManager.h"


auto operator ""_MB(unsigned long long const x) -> long
{
    return 1024L * 1024L * x;
}

namespace armarx::vision::core
{
    shared_memory::SharedMemoryData&
    VisualDataDataManager::getSharedMemoryBuffer(const std::string& instanceName)
    {
        auto& buffer = sharedMemoryProvider->getMemoryBuffer();
        std::hash<std::string> hash{};
        auto it = buffer.find(hash(instanceName));
        ARMARX_CHECK(it != buffer.end());
        return it->second;
    }

    itfc::BlobWithMetaInfo& VisualDataDataManager::getIceBuffer(const std::string& instanceName)
    {
        // TODO: Fix race condition for copying of ice buffers
        auto it = latestVisualData.find(instanceName);
        ARMARX_CHECK(it != latestVisualData.end());
        return it->second;
    }

    void VisualDataDataManager::addVisualData(const std::string& instanceName, const arondto::DataInstanceDescription& type)
    {
        latestVisualData.emplace(instanceName, itfc::BlobWithMetaInfo());
        sharedMemoryProvider->addVisualData(instanceName);
        metaInfo.visualData.emplace(instanceName, type);
        ARMARX_CHECK_EQUAL(sharedMemoryProvider->getMemoryBuffer().size(), latestVisualData.size());
        ARMARX_CHECK_EQUAL(metaInfo.visualData.size(), latestVisualData.size());
    }

    VisualDataDataManager::VisualDataDataManager(const std::string& memoryName,
                                                   const arondto::ProviderMetaInfo& providerMetaInfo)
    {
        // provider types will be added via addVisualData
        sharedMemoryProvider = std::make_unique<SharedMemoryProviderT>(memoryName + ".SharedMemoryProvider", 100_MB);
        for (const auto& type: providerMetaInfo.visualData)
        {
            addVisualData(type.first, type.second);
        }

    }

    void VisualDataDataManager::notifySharedMemoryUpdated(const std::string& instanceName)
    {
        sharedMemoryProvider->notifyUpdated(instanceName);
    }

    const itfc::BlobsWithMetaInfo& VisualDataDataManager::getLatestIceBuffers()
    {
        return latestVisualData;
    }
}