/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       19.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <boost/preprocessor/seq/enum.hpp>
#include <boost/preprocessor/seq/variadic_seq_to_seq.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/seq/rest_n.hpp>

#include <vision/core/aron/VisualDataType.aron.generated.h>

#define ARMARX_VISION_CORE_ARONDTO_VISUALDATATYPE \
(armarx::vision::core::arondto::VisualDataType::Image,cv::Mat) \
(armarx::vision::core::arondto::VisualDataType::DepthImage,cv::Mat) \
(armarx::vision::core::arondto::VisualDataType::RGBDImage,cv::Mat)  \
(armarx::vision::core::arondto::VisualDataType::HSVImage,cv::Mat)   \
(armarx::vision::core::arondto::VisualDataType::GrayScaleImage,cv::Mat) \
(armarx::vision::core::arondto::VisualDataType::PointCloudXYZ,pcl::PointCloud<pcl::PointXYZ>) \
(armarx::vision::core::arondto::VisualDataType::PointCloudXYZI,pcl::PointCloud<pcl::PointXYZI>) \
(armarx::vision::core::arondto::VisualDataType::PointCloudXYZL,pcl::PointCloud<pcl::PointXYZL>)\
(armarx::vision::core::arondto::VisualDataType::PointCloudXYZLNormal,pcl::PointCloud<pcl::PointXYZLNormal>) \
(armarx::vision::core::arondto::VisualDataType::PointCloudXYZRGB,pcl::PointCloud<pcl::PointXYZRGB>)         \
(armarx::vision::core::arondto::VisualDataType::PointCloudXYZRGBA,pcl::PointCloud<pcl::PointXYZRGBA>)       \
(armarx::vision::core::arondto::VisualDataType::PointCloudXYZRGBL,pcl::PointCloud<pcl::PointXYZRGBL>)       \
(armarx::vision::core::arondto::VisualDataType::PointCloudXYZRGBNormal,pcl::PointCloud<pcl::PointXYZRGBNormal>) \
(armarx::vision::core::arondto::VisualDataType::PrincipalCurvatures,pcl::PointCloud<pcl::PrincipalCurvatures>)

#define ENUM_VALUE(elem) \
    BOOST_PP_TUPLE_ELEM(0, elem)

#define ENUM_TYPE(elem) \
    BOOST_PP_TUPLE_ELEM(1, elem)

#define GET_ENUM_VALUE(r, data, elem) \
    (ENUM_VALUE(elem))

#define GET_TYPE(r, data, elem) \
    (ENUM_TYPE(elem))

#define ENUM_VALUE_SEQ \
BOOST_PP_SEQ_FOR_EACH(GET_ENUM_VALUE,_, BOOST_PP_VARIADIC_SEQ_TO_SEQ(ARMARX_VISION_CORE_ARONDTO_VISUALDATATYPE))

#define TYPE_SEQ \
(cv::Mat)        \
(pcl::PointCloud<pcl::PointXYZ>) \
(pcl::PointCloud<pcl::PointXYZI>)\
(pcl::PointCloud<pcl::PointXYZL>)\
(pcl::PointCloud<pcl::PointXYZLNormal>) \
(pcl::PointCloud<pcl::PointXYZRGB>)     \
(pcl::PointCloud<pcl::PointXYZRGBA>)    \
(pcl::PointCloud<pcl::PointXYZRGBL>)    \
(pcl::PointCloud<pcl::PointXYZRGBNormal>) \
(pcl::PointCloud<pcl::PrincipalCurvatures>)


#define EXPLICIT_INSTANTIATION_DECLARATION_impl(r, tuple, interface)              \
extern template BOOST_PP_TUPLE_ELEM(2, 0, tuple) BOOST_PP_TUPLE_ELEM(2, 1, tuple)<interface>;

#define EXPLICIT_INSTANTIATION_DECLARATION(template_type, class_name)                              \
BOOST_PP_SEQ_FOR_EACH(EXPLICIT_INSTANTIATION_DECLARATION_impl, (template_type, class_name), ENUM_VALUE_SEQ) \
static_assert(true, "Force trailing semicolon")

#define EXPLICIT_INSTANTIATION_impl(r, tuple, interface)                          \
template BOOST_PP_TUPLE_ELEM(2, 0, tuple) BOOST_PP_TUPLE_ELEM(2, 1, tuple)<interface>;

#define EXPLICIT_INSTANTIATION(template_type, class_name)                                          \
BOOST_PP_SEQ_FOR_EACH(EXPLICIT_INSTANTIATION_impl, (template_type, class_name), ENUM_VALUE_SEQ) \
static_assert(true, "Force trailing semicolon")

#define FOR_EACH_ENUM_VALUE(macro) \
BOOST_PP_SEQ_FOR_EACH(macro , _, ENUM_VALUE_SEQ) \
static_assert(true, "Force trailing semicolon")

#define FOR_EACH_TYPE(macro) \
BOOST_PP_SEQ_FOR_EACH(macro , _, TYPE_SEQ) \
static_assert(true, "Force trailing semicolon")

#define FOR_EACH_ENUM_PAIR(macro) \
BOOST_PP_SEQ_FOR_EACH(macro , _, BOOST_PP_VARIADIC_SEQ_TO_SEQ(ARMARX_VISION_CORE_ARONDTO_VISUALDATATYPE)) \
static_assert(true, "Force trailing semicolon")

#define EXPLICIT_SPECIALIZATION_DECLARATION_impl(r, tuple, interface)              \
template<> BOOST_PP_TUPLE_ELEM(2, 0, tuple) BOOST_PP_TUPLE_ELEM(2, 1, tuple)<interface>;

#define EXPLICIT_SPECIALIZATION_DECLARATION(template_type, class_name)                              \
BOOST_PP_SEQ_FOR_EACH(EXPLICIT_SPECIALIZATION_DECLARATION_impl, (template_type, class_name), ENUM_VALUE_SEQ) \
static_assert(true, "Force trailing semicolon")

#define DEFINE_VisualData_VARIANT_impl(r, data, elem) BOOST_PP_TUPLE_ELEM(2, 1, elem)

#define DEFINE_VisualData_VARIANT() \
using VisualData = std::variant<BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_FOR_EACH(GET_TYPE,_, BOOST_PP_VARIADIC_SEQ_TO_SEQ(ARMARX_VISION_CORE_ARONDTO_VISUALDATATYPE)))>

#define FOR_EACH_POINTCLOUD(macro) \
BOOST_PP_SEQ_FOR_EACH(macro, _, BOOST_PP_SEQ_REST_N(5,  BOOST_PP_VARIADIC_SEQ_TO_SEQ(ARMARX_VISION_CORE_ARONDTO_VISUALDATATYPE))) \
static_assert(true, "Force trailing semicolon")

