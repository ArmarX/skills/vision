/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       17.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BlobWithMetaInfo.h"

namespace armarx::vision::core
{
    BlobWithMetaInfo
    BlobWithMetaInfo::fromIce(const itfc::BlobWithMetaInfo& blobWithMetaInfo)
    {
        arondto::DataInstanceMetaInfo metaInfo;
        metaInfo.fromAron(blobWithMetaInfo.metaInfo);
        return {.metaInfo=metaInfo, .blob=blobWithMetaInfo.blob};
    }

    itfc::BlobWithMetaInfo
    BlobWithMetaInfo::toIce(const BlobWithMetaInfo& blobWithMetaInfo)
    {
        return {.blob=blobWithMetaInfo.blob, .metaInfo=blobWithMetaInfo.metaInfo.toAronDTO()};
    }
}