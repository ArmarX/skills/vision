/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       04.10.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <vision/core/ForwardDeclarations.h>
#include <vision/core/aron/ProviderMetaInfo.aron.generated.h>

namespace armarx::vision::core
{
    class VisualDataDataManager
    {
        using SharedMemoryProviderT = shared_memory::SharedMemoryProvider;
    public:

        // Shared Memory
        shared_memory::SharedMemoryData& getSharedMemoryBuffer(const std::string& instanceName);
        void notifySharedMemoryUpdated(const std::string& instanceName);

        // Ice
        itfc::BlobWithMetaInfo& getIceBuffer(const std::string& instanceName);
        const itfc::BlobsWithMetaInfo& getLatestIceBuffers();

        // General
        void addVisualData(const std::string& instanceName, const arondto::DataInstanceDescription& type);
        VisualDataDataManager(const std::string& memoryName, const arondto::ProviderMetaInfo& providerMetaInfo);

    private:
        std::map<std::string, itfc::BlobWithMetaInfo> latestVisualData;
        std::unique_ptr<SharedMemoryProviderT> sharedMemoryProvider;
        arondto::ProviderMetaInfo metaInfo;
    };
}