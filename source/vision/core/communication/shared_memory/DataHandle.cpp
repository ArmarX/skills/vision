/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       27.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DataHandle.h"

#include <opencv2/core/mat.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <vision/core/aron/DataInstanceMetaInfo.aron.generated.h>


#define EXPLICIT_ENCODER_DECODER_INSTANTIATION(r, data, elem) \
template struct DataEncoder<elem>;                      \
template struct DataDecoder<elem>;

#define EXPLICIT_DATA_HANDLE_INSTANTIATION(r, data, elem) \
template struct DataHandle<elem, std::vector<std::byte>>; \
template struct DataHandle<elem, core::shared_memory::SharedMemoryData::VectorT>; \
template struct DataHandle<elem, const core::shared_memory::SharedMemoryData::VectorT>;

namespace armarx::vision::core::communication::shared_memory
{
    auto total_size = [](const arondto::DataInstanceMetaInfo& info) -> int
    {
        return info.size.height * info.size.width * info.size.bytesPerElement;
    };

    template <typename T> const std::multimap<int, VisualDataT> DataEncoder<T>::matTypeToEnum = {};
    template <> const std::multimap<int, VisualDataT> DataEncoder<
            cv::Mat>::matTypeToEnum = {{CV_32FC3, VisualDataT::Image},
                                       {CV_16UC3, VisualDataT::Image},
                                       {CV_8UC3,  VisualDataT::Image},
                                       {CV_32FC1, VisualDataT::DepthImage},
                                       {CV_16UC1, VisualDataT::GrayScaleImage},
                                       {CV_8UC1,  VisualDataT::GrayScaleImage},
                                       {CV_32FC4, VisualDataT::RGBDImage},
                                       {CV_16UC4, VisualDataT::RGBDImage},
                                       {CV_8UC4,  VisualDataT::RGBDImage},
                                       {CV_32FC3, VisualDataT::HSVImage}};


    template <>
    void
    DataEncoder<cv::Mat>::bit_cast(const cv::Mat& data, std::byte* buffer, const arondto::DataInstanceMetaInfo& info)
    {
        // TODO: thread locks when allocating an exception
        ARMARX_CHECK_EQUAL(data.rows, info.size.height);
        ARMARX_CHECK_EQUAL(data.cols, info.size.width);
        ARMARX_CHECK_EQUAL(data.elemSize(), info.size.bytesPerElement);
        ARMARX_CHECK(matTypeToEnum.find(data.type()) != matTypeToEnum.end());
        auto range = matTypeToEnum.equal_range(data.type());
        bool isInRange = false;
        for (auto it = range.first; it != range.second; it++)
        {
            if (info.type == it->second) isInRange = true;
        }
        ARMARX_CHECK(isInRange);
        std::memcpy(buffer, data.data, total_size(info));
    }

    template <typename T> const std::map<std::pair<VisualDataT, int>, int> DataDecoder<T>::enumToMatType = {};
    template <> const std::map<std::pair<VisualDataT, int>, int> DataDecoder<
            cv::Mat>::enumToMatType = {{{VisualDataT::Image,          12}, CV_32FC3},
                                       {{VisualDataT::Image,          6},  CV_16UC3},
                                       {{VisualDataT::Image,          3},  CV_8UC3},
                                       {{VisualDataT::DepthImage,     4},  CV_32FC1},
                                       {{VisualDataT::GrayScaleImage, 2},  CV_16UC1},
                                       {{VisualDataT::GrayScaleImage, 1},  CV_8UC1},
                                       {{VisualDataT::RGBDImage,      16}, CV_32FC4},
                                       {{VisualDataT::RGBDImage,      8},  CV_16UC4},
                                       {{VisualDataT::RGBDImage,      4},  CV_8UC4},
                                       {{VisualDataT::HSVImage,       12}, CV_32FC3}};

    template <>
    cv::Mat DataDecoder<cv::Mat>::bit_cast(const std::byte* buffer, const arondto::DataInstanceMetaInfo& info)
    {
        assert(enumToMatType.find(std::make_pair(info.type.value, info.size.bytesPerElement)) != enumToMatType.end());
        cv::Mat image(info.size.height, info.size.width,
                      enumToMatType.at(std::make_pair(info.type.value, info.size.bytesPerElement)));
        std::memcpy(image.data, buffer, total_size(info));
        return image;
    }

    template <typename DataT, typename BufferT>
    typename DataHandle<DataT, BufferT>::DataT DataHandle<DataT, BufferT>::decodeImpl() const
    {
        ARMARX_CHECK_GREATER_EQUAL(buffer.size(), total_size(metaInfo));
        return DataDecoder<DataT>::bit_cast(buffer.data(), metaInfo);
    }

    template <typename T>
    void DataEncoder<pcl::PointCloud<T>>::bit_cast(const pcl::PointCloud<T>& data, std::byte* buffer,
                                                   const arondto::DataInstanceMetaInfo& info)
    {
        static_assert(std::is_trivially_copyable_v<T>);
        ARMARX_CHECK(info.type.value == traits::PointCloud<pcl::PointCloud<T>>::enum_value);
        ARMARX_CHECK_EQUAL(data.width, info.size.width);
        ARMARX_CHECK_EQUAL(data.height, info.size.height);
        ARMARX_CHECK_EQUAL(sizeof(T), info.size.bytesPerElement);
        std::memcpy(buffer, data.points.data(), total_size(info));
    }

    template <typename T>
    pcl::PointCloud<T>
    DataDecoder<pcl::PointCloud<T>>::bit_cast(const std::byte* buffer, const arondto::DataInstanceMetaInfo& info)
    {
        static_assert(std::is_trivially_copyable_v<T>);
        pcl::PointCloud<T> cloud(info.size.width, info.size.height);
        ARMARX_CHECK(info.type.value == traits::PointCloud<pcl::PointCloud<T>>::enum_value);
        ARMARX_CHECK_EQUAL(cloud.size(), info.size.width * info.size.height);
        ARMARX_CHECK_EQUAL(total_size(info), sizeof(T) * cloud.size());
        // reinterpret_cast is here to silence the warning about copying of non-trivial type PointT from std::byte
        // TODO: is this actually safe and defined behavior? It works, and it should be ok because the point types are
        //  eigen aligned
        std::memcpy(reinterpret_cast<std::byte*>(cloud.points.data()), buffer, total_size(info));
        return cloud;
    }

    FOR_EACH_TYPE(EXPLICIT_ENCODER_DECODER_INSTANTIATION);
    FOR_EACH_TYPE(EXPLICIT_DATA_HANDLE_INSTANTIATION);
}