/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       27.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vision/core/Traits.h>
#include <vision/core/shared_memory/SharedMemoryData.h>
#include <vision/core/communication/CommunicationHandleBase.h>
#include <vision/core/aron/DataInstanceMetaInfo.aron.generated.h>

#define EXPLICIT_ENCODER_DECODER_INSTANTIATION_DECLARATION(r, data, elem) \
extern template struct DataEncoder<elem>;                      \
extern template struct DataDecoder<elem>;

#define EXPLICIT_DATA_HANDLE_INSTANTIATION_DECLARATION(r, data, elem) \
extern template struct DataHandle<elem, std::vector<std::byte>>; \
extern template struct DataHandle<elem, core::shared_memory::SharedMemoryData::VectorT>; \
extern template struct DataHandle<elem, const core::shared_memory::SharedMemoryData::VectorT>;

namespace armarx::vision::core::communication::shared_memory
{

    template <typename T>
    struct DataEncoder
    {
        static const std::multimap<int, VisualDataT> matTypeToEnum;

        static void bit_cast(const T&, std::byte*, const arondto::DataInstanceMetaInfo&);
    };

    template <typename T>
    struct DataDecoder
    {
        static const std::map<std::pair<VisualDataT, int>, int> enumToMatType;

        static T bit_cast(const std::byte*, const arondto::DataInstanceMetaInfo&);
    };

    template <typename Data, typename Buffer>
    struct DataHandle : public CommunicationHandleBase<Data, Buffer, DataHandle<Data, Buffer>>
    {
        using BaseT = CommunicationHandleBase<Data, Buffer, DataHandle<Data, Buffer>>;
        using DataT = typename BaseT::DataT;
        using BufferT = typename BaseT::BufferT;

        DataHandle(Buffer& buffer, const arondto::DataInstanceMetaInfo& info) :
                BaseT(buffer),
                metaInfo(info),
                dataSize(info.size.height * info.size.width * info.size.bytesPerElement)
        {
            ARMARX_CHECK_GREATER(dataSize, 0);
            if constexpr (not std::is_const_v<BufferT>)
            {
                if (buffer.size() < dataSize)
                {
                    buffer.resize(dataSize);
                }
            }
            assert(buffer.size() >= dataSize);
            // TODO: accessing buffer.size() freezes the program
            //            ARMARX_CHECK_GREATER_EQUAL(buffer.size(), dataSize);
        };

    private:
        template <typename U = Buffer, typename std::enable_if_t<not std::is_const_v<U>, int> = 0>
        void encodeImpl(const DataT& data) const
        {
            DataEncoder<DataT>::bit_cast(data, buffer.data(), metaInfo);
        }

        DataT decodeImpl() const;

        friend BaseT;
        using BaseT::buffer;
        const arondto::DataInstanceMetaInfo& metaInfo;
        const std::size_t dataSize;
    };

    // To circumvent partial CTAD
    template <typename DataT, typename BufferT>
    void
    encode(BufferT& buffer, const DataT& data, const arondto::DataInstanceMetaInfo& info)
    {
//        ARMARX_CHECK(T == info.type.value);
        DataHandle<DataT, BufferT>(buffer, info).encode(data);
    }

    template <typename DataT, typename BufferT>
    DataT decode(BufferT& buffer, const arondto::DataInstanceMetaInfo& info)
    {
//        ARMARX_CHECK(T == info.type.value);
        return DataHandle<DataT, BufferT>(buffer, info).decode();
    }

    template <>
    void DataEncoder<cv::Mat>::bit_cast(const cv::Mat&, std::byte*, const arondto::DataInstanceMetaInfo&);

    template <>
    cv::Mat DataDecoder<cv::Mat>::bit_cast(const std::byte*, const arondto::DataInstanceMetaInfo&);

    template <typename T>
    struct DataEncoder<pcl::PointCloud<T>>
    {
        static void bit_cast(const pcl::PointCloud<T>&, std::byte*, const arondto::DataInstanceMetaInfo&);
    };

    template <typename T>
    struct DataDecoder<pcl::PointCloud<T>>
    {
        static pcl::PointCloud<T> bit_cast(const std::byte*, const arondto::DataInstanceMetaInfo&);
    };

    FOR_EACH_TYPE(EXPLICIT_ENCODER_DECODER_INSTANTIATION_DECLARATION);
    FOR_EACH_TYPE(EXPLICIT_DATA_HANDLE_INSTANTIATION_DECLARATION);
}

#undef EXPLICIT_DATA_HANDLE_INSTANTIATION_DECLARATION
#undef EXPLICIT_ENCODER_DECODER_INSTANTIATION_DECLARATION