/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       29.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MetaInfoHandle.h"
#include <vision/core/aron/DataInstanceMetaInfo.aron.generated.h>

namespace armarx::vision::core::communication::shared_memory
{
    template <typename BufferT>
    typename MetaInfoHandle<BufferT>::DataT MetaInfoHandle<BufferT>::decodeImpl() const
    {
        std::string data_as_string;
        data_as_string.resize(buffer.size());
        std::memcpy(data_as_string.data(), buffer.data(), buffer.size());
        auto json = nlohmann::json::parse(data_as_string);
        DataT data;
        data.fromAron(aron::converter::AronNlohmannJSONConverter::ConvertFromNlohmannJSONObject(json));
        return data;
    }

    template
    struct MetaInfoHandle<std::vector<std::byte>>;
    template
    struct MetaInfoHandle<core::shared_memory::SharedMemoryData::VectorT>;
    template
    struct MetaInfoHandle<const core::shared_memory::SharedMemoryData::VectorT>;

}