/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       29.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vision/core/communication/CommunicationHandleBase.h>
#include <vision/core/shared_memory/SharedMemoryData.h>
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>
#include <vision/core/ForwardDeclarations.h>

namespace armarx::vision::core::communication::shared_memory
{
    template <typename Buffer>
    struct MetaInfoHandle : public CommunicationHandleBase<arondto::DataInstanceMetaInfo, Buffer,
                                                                       MetaInfoHandle<Buffer>>
    {
        using BaseT = CommunicationHandleBase<arondto::DataInstanceMetaInfo, Buffer, MetaInfoHandle<Buffer>>;
        using BufferT = typename BaseT::BufferT;
        using DataT = typename BaseT::DataT;

        explicit MetaInfoHandle(Buffer& b) : BaseT(b)
        {
        };

    private:
        friend BaseT;

        using BaseT::buffer;
        template <typename U = Buffer, typename std::enable_if_t<not std::is_const_v<U>, int> = 0>
        void encodeImpl(const DataT& metaInfo) const
        {
            auto json = aron::converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(metaInfo.toAron());
            auto data_as_string = json.dump();
            if (buffer.size() < data_as_string.size())
            {
                buffer.resize(data_as_string.size());
            }
            assert(buffer.size() >= data_as_string.size());
            std::memcpy(buffer.data(), data_as_string.data(), data_as_string.size());
        }

        [[nodiscard]] DataT decodeImpl() const;

    };
    extern template struct MetaInfoHandle<std::vector<std::byte>>;
    extern template struct MetaInfoHandle<core::shared_memory::SharedMemoryData::VectorT>;
    extern template struct MetaInfoHandle<const core::shared_memory::SharedMemoryData::VectorT>;
}
