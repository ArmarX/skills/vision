/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       29.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vision/core/Traits.h>
#include <vision/core/BlobWithMetaInfo.h>
#include <vision/core/communication/CommunicationHandleBase.h>

#define EXPLICIT_DATA_HANDLE_INSTANTIATION_DECLARATION(r, data, elem) \
extern template struct DataHandle<elem>;

namespace armarx::vision::core::communication::ice
{

    template <typename T>
    struct DataEncoder
    {
        static void encode(const T&, armarx::Blob&, const arondto::DataInstanceMetaInfo&);
    };

    template <typename T>
    struct DataDecoder
    {
        static T decode(const armarx::Blob&, const arondto::DataInstanceMetaInfo&);
    };

    template <typename Data>
    struct DataHandle : CommunicationHandleBase<Data, armarx::Blob, DataHandle<Data>>
    {
        using BaseT = CommunicationHandleBase<Data, armarx::Blob, DataHandle<Data>>;
        using DataT = typename BaseT::DataT;
        using BufferT = typename BaseT::BufferT;
        DataHandle(BlobWithMetaInfo& blob) : BaseT(blob.blob), metaInfo(blob.metaInfo) {
            if constexpr (not std::is_const_v<BufferT>)
            {
                auto dataSize = metaInfo.size.height * metaInfo.size.width * metaInfo.size.bytesPerElement;
                if (buffer.size() < static_cast<std::size_t>(dataSize))
                {
                    buffer.resize(dataSize);
                }
                assert(buffer.size() >= static_cast<std::size_t>(dataSize));
            }
            // buffer resizing is not done here, as cv::imencode resizes the buffer automatically
        };

    private:
        void encodeImpl(const DataT& data) const;

        DataT decodeImpl() const;

        using BaseT::buffer;
        friend BaseT;
        const arondto::DataInstanceMetaInfo& metaInfo;
    };

    template <typename DataT>
    void
    encode(BlobWithMetaInfo& buffer, const DataT& data)
    {
//        ARMARX_CHECK(T == buffer.metaInfo.type.value);
        DataHandle<DataT>(buffer).encode(data);
    }

    template <typename DataT>
    DataT decode(BlobWithMetaInfo& buffer)
    {
//        ARMARX_CHECK(T == buffer.metaInfo.type.value);
        return DataHandle<DataT>(buffer).decode();
    }

    FOR_EACH_TYPE(EXPLICIT_DATA_HANDLE_INSTANTIATION_DECLARATION);
}
#undef EXPLICIT_DATA_HANDLE_INSTANTIATION_DECLARATION