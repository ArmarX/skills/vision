/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       29.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DataHandle.h"
#include <opencv2/imgcodecs.hpp>
#include <pcl/compression/octree_pointcloud_compression.h>
// because pcl is stupid and assumes it makes sense to hide implementation "somewhere where everybody could find it"
#include <pcl/io/impl/octree_pointcloud_compression.hpp>
#include <vision/core/communication/shared_memory/DataHandle.h>

#define EXPLICIT_DATA_HANDLE_INSTANTIATION(r, data, elem) \
template struct DataHandle<elem>;

namespace armarx::vision::core::communication::ice
{
    auto total_size = [](const arondto::DataInstanceMetaInfo& info) -> int
    {
        return info.size.height * info.size.width * info.size.bytesPerElement;
    };

    template <typename DataT>
    typename DataHandle<DataT>::DataT DataHandle<DataT>::decodeImpl() const
    {
        ARMARX_CHECK_NOT_EMPTY(buffer);
        return DataDecoder<DataT>::decode(buffer, metaInfo);
    }

    template <typename DataT>
    void DataHandle<DataT>::encodeImpl(const DataT& data) const
    {
        DataEncoder<DataT>::encode(data, buffer, metaInfo);
    }


    template <>
    cv::Mat DataDecoder<cv::Mat>::decode(const armarx::Blob& buffer, const arondto::DataInstanceMetaInfo& metaInfo)
    {
        // TODO: imencode/decode transform 32FC3 to 8UC3
        auto img = cv::imdecode(buffer, cv::IMREAD_UNCHANGED);
        ARMARX_CHECK_EQUAL(img.size().height, metaInfo.size.height);
        ARMARX_CHECK_EQUAL(img.size().width, metaInfo.size.width);
        //        ARMARX_CHECK_EQUAL(img.elemSize(), metaInfo.size.bytesPerElement);
        return img;
    }

    template <>
    void
    DataEncoder<cv::Mat>::encode(const cv::Mat& data, armarx::Blob& buffer, const arondto::DataInstanceMetaInfo& info)
    {
        ARMARX_CHECK_EQUAL(data.rows, info.size.height);
        ARMARX_CHECK_EQUAL(data.cols, info.size.width);
        ARMARX_CHECK_EQUAL(data.elemSize(), info.size.bytesPerElement);
        std::vector<int> compression_params{cv::IMWRITE_PNG_COMPRESSION, arondto::CompressionQuality::EnumToValueMap
                                                                                 .at(info.compressionQuality.value) - 1,
                                            cv::IMWRITE_PNG_STRATEGY, cv::IMWRITE_PNG_STRATEGY_RLE};
        cv::imencode("*.png", data, buffer, compression_params);
    }

    template <typename PointT>
    struct DataEncoder<pcl::PointCloud<PointT>>
    {
        static void
        encode(const pcl::PointCloud<PointT>& data, armarx::Blob& buffer, const arondto::DataInstanceMetaInfo& info)
        {
            ARMARX_CHECK_GREATER_EQUAL(buffer.size(), total_size(info));
            if constexpr (std::is_same_v<PointT, pcl::PrincipalCurvatures>)
            {
                throw armarx::NotImplementedYetException(
                        "Ice Encoding is not implemented for pcl::PrincipalCurvatures yet");
            } else
            {
                pcl::io::compression_Profiles_e compressionProfile;
                switch (info.compressionQuality.value)
                {
                    case arondto::CompressionQuality::High:
                        compressionProfile = pcl::io::HIGH_RES_ONLINE_COMPRESSION_WITH_COLOR;
                        break;
                    case arondto::CompressionQuality::Medium:
                        compressionProfile = pcl::io::MED_RES_ONLINE_COMPRESSION_WITH_COLOR;
                        break;
                    case arondto::CompressionQuality::Low:
                        compressionProfile = pcl::io::LOW_RES_ONLINE_COMPRESSION_WITH_COLOR;
                        break;
                }
//                auto pointCloudEncoder = pcl::io::OctreePointCloudCompression<PointT>(compressionProfile, true);
                // TODO: maybe use instead            auto pointCloudEncoder = pcl::io::OrganizedPointCloudCompression<PointT>();
                std::stringstream encodedData;
                // TODO: there is a memory leak in this function
//                pointCloudEncoder.encodePointCloud(typename pcl::PointCloud<PointT>::ConstPtr(&data), encodedData);
                //                std::memcpy(buffer.data(), encodedData.str().c_str(), info.size.width * info.size.height);
                shared_memory::DataEncoder<pcl::PointCloud<PointT>>::bit_cast(data, reinterpret_cast<std::byte*>(buffer.data()), info);
            }
        }
    };

    template <typename PointT>
    struct DataDecoder<pcl::PointCloud<PointT>>
    {
        static pcl::PointCloud<PointT> decode(const armarx::Blob& buffer, const arondto::DataInstanceMetaInfo& metaInfo)
        {

            if constexpr (std::is_same_v<PointT, pcl::PrincipalCurvatures>)
            {
                throw armarx::NotImplementedYetException(
                        "Ice Decoding is not implemented for pcl::PrincipalCurvatures yet");
            } else
            {
                ARMARX_CHECK_NOT_EMPTY(buffer);
//                auto pointCloudDecoder = pcl::io::OctreePointCloudCompression<PointT>();
                // TODO: maybe use instead            auto pointCloudEncoder = pcl::io::OrganizedPointCloudCompression<PointT>();
//                std::stringstream compressedData;
//                typename pcl::PointCloud<PointT>::Ptr pointCloud = boost::make_shared<pcl::PointCloud<PointT>>();
//                compressedData.str().assign(buffer.data(), buffer.data() + total_size(metaInfo));
//                pointCloudDecoder.decodePointCloud(compressedData, pointCloud);
                return shared_memory::DataDecoder<pcl::PointCloud<PointT>>::bit_cast(reinterpret_cast<const std::byte*>(buffer.data()), metaInfo);
            }
        }

    };

    FOR_EACH_TYPE(EXPLICIT_DATA_HANDLE_INSTANTIATION);
}