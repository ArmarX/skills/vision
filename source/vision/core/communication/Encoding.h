/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       20.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vision/core/communication/shared_memory/DataHandle.h>
#include <vision/core/communication/shared_memory/MetaInfoHandle.h>
#include <vision/core/communication/ice/DataHandle.h>

namespace armarx::vision::core::communication
{
    template <typename DataT>
    void encode(core::shared_memory::SharedMemoryData& buffer, const DataT& data,
                const arondto::DataInstanceMetaInfo& info)
    {
        namespace bi = boost::interprocess;
        bi::scoped_lock<bi::interprocess_upgradable_mutex> exclusive_lock(buffer.readWriteMutex);
        shared_memory::encode(buffer.data, data, info);
        shared_memory::MetaInfoHandle(buffer.info).encode(info);
        buffer.updateTimestampInMicroS = info.provisionTimestamp.toMicroSecondsSinceEpoch();
    }

    template <typename DataT>
    void encode(BlobWithMetaInfo& buffer, const DataT& data,
                const arondto::DataInstanceMetaInfo& info)
    {
        buffer.metaInfo = info;
        ice::encode(buffer, data);
    }
}