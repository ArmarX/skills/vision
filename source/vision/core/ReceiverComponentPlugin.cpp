/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       22.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <opencv2/core/mat.hpp>
#include <ArmarXCore/core/ArmarXManager.h>
#include <vision/core/aron/DataInstanceMetaInfo.aron.generated.h>

#include "ReceiverComponentPlugin.h"

namespace armarx::vision::core
{
    template <typename DataT>
    DataT ReceiverComponentPlugin::getVisualData(const std::string& providerName,
                                                  const std::string& instanceName, const std::optional<armarx::DateTime>& newerThan) const
    {

        return getCameraClient(providerName).getVisualData<DataT>(instanceName, newerThan);
    }

    void ReceiverComponentPlugin::addConsumer(const std::string& providerName)
    {
        auto item = blobConsumerMap.emplace(providerName, IceInternal::Handle<CameraClient>(
                new CameraClient(&parent(), providerName)));
        parent().getArmarXManager()->addObject(item.first->second);
    }

    ReceiverComponentPlugin::ReceiverComponentPlugin(ManagedIceObject& parent, const std::string& prefix) :
            ComponentPlugin(parent, prefix)
    {
    }

    void ReceiverComponentPlugin::preOnConnectComponent()
    {
    }

    arondto::DataInstanceMetaInfo
    ReceiverComponentPlugin::getMetaInfo(const std::string& providerName, const std::string& instanceName,
                                         const std::optional<armarx::DateTime>& newerThan) const
    {
        return getCameraClient(providerName).getMetaInfo(instanceName, newerThan);
    }

    template <typename DataT>
    std::pair<DataT, arondto::DataInstanceMetaInfo>
    ReceiverComponentPlugin::getVisualDataAndMetaInfo(const std::string& providerName,
                                                       const std::string& instanceName, const std::optional<armarx::DateTime>& newerThan) const
    {
        return getCameraClient(providerName).getVisualDataAndMetaInfo<DataT>(instanceName, newerThan);
    }

    template <typename ...DataTs>
    std::tuple<DataWithMetaInfo<DataTs>...>
    ReceiverComponentPlugin::getVisualDataAndMetaInfo(const std::string& providerName,
                                                        const std::vector<std::string>& instanceNames,
                                                        const std::optional<armarx::DateTime>& newerThan) const
    {
        auto requestTime = armarx::core::time::DateTime::Now();
        return getCameraClient(providerName).getVisualDataAndMetaInfo<DataTs...>(instanceNames, newerThan);
    }


    CameraClient& ReceiverComponentPlugin::getCameraClient(const std::string& providerName) const
    {
        auto blobConsumer = blobConsumerMap.find(providerName);
        if (blobConsumer == blobConsumerMap.end())
        {
            if (providerName.empty() && blobConsumerMap.size() == 1)
            {
                blobConsumer = blobConsumerMap.begin();
            } else
            {
                throw armarx::InvalidArgumentException("Provider with name " + providerName + " does not exist!");
            }
        }
        return *(blobConsumer->second);
    }

    cv::Mat ReceiverComponentPlugin::getImage(const std::string& providerName, const std::string& instanceName,
                                              const std::optional<armarx::DateTime>& newerThan) const
    {
        return getVisualData<cv::Mat>(providerName, instanceName, newerThan);
    }

    std::pair<cv::Mat, arondto::DataInstanceMetaInfo>
    ReceiverComponentPlugin::getImageAndMetaInfo(const std::string& providerName, const std::string& instanceName,
                                                 const std::optional<armarx::DateTime>& newerThan) const
    {
        return getVisualDataAndMetaInfo<cv::Mat>(providerName, instanceName, newerThan);
    }

    template std::tuple<DataWithMetaInfo<cv::Mat>, DataWithMetaInfo<cv::Mat>>
        ReceiverComponentPlugin::getVisualDataAndMetaInfo<cv::Mat, cv::Mat>(const std::string&,
                                                                                        const std::vector<std::string>&,
                                                                                        const std::optional<
                                                                                                armarx::DateTime>&) const;
}
//
//namespace armarx
//{
//    CameraConsumerComponentPluginUser::CameraConsumerComponentPluginUser()
//    {
//        addPlugin(cameraConsumerComponentPlugin, "");
//    }
//
//    void CameraConsumerComponentPluginUser::addConsumer(const std::string& providerName)
//    {
//        cameraConsumerComponentPlugin->addConsumer(providerName);
//    }
//
//    cv::Mat
//    CameraConsumerComponentPluginUser::getImage(const std::string& providerName, const std::string& instanceName,
//                                                const std::optional<armarx::DateTime>& newerThan) const
//    {
//        return cameraConsumerComponentPlugin->getVisualData<cv::Mat>(providerName, instanceName, newerThan);
//    }
//
//    vision::core::arondto::DataInstanceMetaInfo
//    CameraConsumerComponentPluginUser::getMetaInfo(const std::string& providerName, const std::string& instanceName,
//                                                   const std::optional<armarx::DateTime>& newerThan) const
//    {
//        return cameraConsumerComponentPlugin->getMetaInfo(providerName, instanceName, newerThan);
//    }
//
//    std::pair<cv::Mat, vision::core::arondto::DataInstanceMetaInfo>
//    CameraConsumerComponentPluginUser::getImageAndMetaInfo(const std::string& providerName,
//                                                           const std::string& instanceName,
//                                                           const std::optional<armarx::DateTime>& newerThan) const
//    {
//        return cameraConsumerComponentPlugin->getVisualDataAndMetaInfo<cv::Mat>(providerName, instanceName, newerThan);
//    }
//
//    template <typename... DataTs>
//    std::tuple<vision::core::DataWithMetaInfo<DataTs>...>
//    CameraConsumerComponentPluginUser::getVisualDataAndMetaInfo(const std::string& providerName,
//                                                                  const std::vector<std::string>& instanceNames,
//                                                                  const std::optional<
//                                                                          armarx::DateTime>& newerThan) const
//    {
//        return cameraConsumerComponentPlugin
//                ->getVisualDataAndMetaInfo<DataTs...>(providerName, instanceNames, newerThan);
//    }
//
//    template std::tuple<vision::core::DataWithMetaInfo<cv::Mat>, vision::core::DataWithMetaInfo<cv::Mat>>
//    CameraConsumerComponentPluginUser::getVisualDataAndMetaInfo<cv::Mat, cv::Mat>(const std::string&,
//                                                                                    const std::vector<std::string>&,
//                                                                                    const std::optional<
//                                                                                            armarx::DateTime>&) const;
//}