/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       28.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// test includes and other stuff
#define BOOST_TEST_MODULE Manipulation::ArmarXLibraries::core
#define ARMARX_BOOST_TEST

#include <armarx/vision/Test.h>
#include <vision/core/Util.h>
#include <vision/core/aron/ProviderMetaInfo.aron.generated.h>
#include <vision/core/communication/shared_memory/DataHandle.h>
#include <vision/core/communication/shared_memory/MetaInfoHandle.h>
#include <vision/core/communication/ice/DataHandle.h>

namespace armarx::vision::core
{

    BOOST_AUTO_TEST_CASE(SHMMetaInfo)
    {
        std::vector<std::byte> buffer(0);
        communication::shared_memory::MetaInfoHandle handle(buffer);
        arondto::DataInstanceMetaInfo info;
        info.size.height = 1020;
        info.size.width = 999;
        info.type = arondto::VisualDataType::PrincipalCurvatures;
        info.compressionQuality = arondto::CompressionQuality::High;
        auto json = aron::converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(info.toAron());
        std::string data_as_string(json.dump());
        arondto::DataInstanceMetaInfo info2;
        auto json2 = nlohmann::json::parse(data_as_string);
        info2.fromAron(aron::converter::AronNlohmannJSONConverter::ConvertFromNlohmannJSONObject(json2));
        BOOST_CHECK(info == info2);
        handle.encode(info);
        auto info3 = handle.decode();
        BOOST_CHECK(info == info3);
    }

    BOOST_AUTO_TEST_CASE(SHMImage)
    {
        std::vector<std::byte> buffer(0);
        arondto::DataInstanceMetaInfo info;
        info.size.width = 10;
        info.size.height = 20;
        cv::Mat image = util::generateRandomImage(info.size.width, info.size.height, CV_32FC3);
        // bytes per element not set
        BOOST_CHECK_THROW(communication::shared_memory::encode(buffer, image, info), armarx::LocalException);
        info.size.bytesPerElement = 12;
        info.type = arondto::VisualDataType::Image;
        communication::shared_memory::encode(buffer, image, info);
        auto image2 = communication::shared_memory::decode<cv::Mat>(buffer, info);
        info.size.width = 300;
        BOOST_CHECK_THROW(communication::shared_memory::encode(buffer, image, info), armarx::LocalException);
        BOOST_CHECK(util::imagesAreEqual(image, image2));
    }

    BOOST_AUTO_TEST_CASE(IceImage)
    {
        BlobWithMetaInfo buffer;
        auto& info = buffer.metaInfo;
        info.size.width = 10;
        info.size.height = 20;
        cv::Mat image = util::generateRandomImage(info.size.width, info.size.height, CV_16UC3);
        // bytes per element not set
        BOOST_CHECK_THROW(communication::ice::encode(buffer, image), armarx::LocalException);
        info.size.bytesPerElement = 6;
        info.type = arondto::VisualDataType::Image;
        communication::ice::encode(buffer, image);
        auto image2 = communication::ice::decode<cv::Mat>(buffer);
        info.size.width = 300;
        BOOST_CHECK_THROW(communication::ice::encode(buffer, image), armarx::LocalException);
        BOOST_CHECK(util::imagesAreEqual(image, image2));
    }

    BOOST_AUTO_TEST_CASE(SHMPointXYZ)
    {
        using T = pcl::PointCloud<pcl::PointXYZ>;
        std::vector<std::byte> buffer(0);
        arondto::DataInstanceMetaInfo info;
        info.size.width = 10;
        info.size.height = 20;
        auto cloud = util::generateRandomPointCloud<pcl::PointXYZ>(info.size.width, info.size.height);
        // bytes per element not set
        BOOST_CHECK_THROW(communication::shared_memory::encode(buffer, cloud, info), armarx::LocalException);
        info.size.bytesPerElement = sizeof(pcl::PointXYZ);
        info.type = arondto::VisualDataType::Image;
        // wrong metaInfo.type
        BOOST_CHECK_THROW(communication::shared_memory::encode(buffer, cloud, info), armarx::LocalException);
        info.type = arondto::VisualDataType::PointCloudXYZ;
        communication::shared_memory::encode(buffer, cloud, info);
        auto cloud2 = communication::shared_memory::decode<T>(buffer, info);
        BOOST_CHECK(util::pointCloudsAreEqual(cloud, cloud2));
        info.size.width = 300;
        BOOST_CHECK_THROW(communication::shared_memory::encode(buffer, cloud, info), armarx::LocalException);
    }

    BOOST_AUTO_TEST_CASE(IcePointXYZ)
    {
        using T = pcl::PointCloud<pcl::PointXYZ>;
        BlobWithMetaInfo buffer;
        buffer.metaInfo.size.width = 10;
        buffer.metaInfo.size.height = 20;
        auto cloud = util::generateRandomPointCloud<pcl::PointXYZ>(buffer.metaInfo.size.width,
                                                                   buffer.metaInfo.size.height);
        // bytes per element not set
        BOOST_CHECK_THROW(communication::ice::encode(buffer, cloud), armarx::LocalException);
        buffer.metaInfo.size.bytesPerElement = sizeof(pcl::PointXYZ);
        buffer.metaInfo.type = arondto::VisualDataType::Image;
        // wrong metaInfo.type
        BOOST_CHECK_THROW(communication::ice::encode(buffer, cloud), armarx::LocalException);
        buffer.metaInfo.type = arondto::VisualDataType::PointCloudXYZ;
        communication::ice::encode(buffer, cloud);
        auto cloud2 = communication::ice::decode<T>(buffer);
        BOOST_CHECK(util::pointCloudsAreEqual(cloud, cloud2));
        buffer.metaInfo.size.width = 300;
        BOOST_CHECK_THROW(communication::ice::encode(buffer, cloud), armarx::LocalException);
    }
}