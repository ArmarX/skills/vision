/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       18.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <opencv2/core/mat.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <vision/core/aron/DataInstanceMetaInfo.aron.generated.h>


#include "ProviderComponentPlugin.h"

#define EXPLICIT_PC_PROVIDE_INSTANTIATION(r, data, elem) \
template                                                      \
void ProviderComponentPlugin::provide<vision::core::traits::PointCloud<ENUM_TYPE(elem)>::point_type>(const ENUM_TYPE(elem)&,\
const std::string&, const std::string&);

namespace armarx::vision::core
{

    template <typename DataT>
    void ProviderComponentPlugin::provide(const DataT& visualData,
                                          const arondto::DataInstanceMetaInfo& info,
                                          const std::string& providerName, const std::string& instanceName)
    {
        ARMARX_TRACE;
        auto blobProvider = blobProviderMap.find(providerName);
        if (blobProvider == blobProviderMap.end())
        {
            ARMARX_TRACE;
            if (providerName.empty() && blobProviderMap.size() == 1)
            {
                ARMARX_TRACE;
                blobProvider = blobProviderMap.begin();
            } else
            {
                ARMARX_TRACE;
                throw armarx::InvalidArgumentException("Provider with name " + providerName + " does not exist!");
            }
        }
        blobProvider->second.first->update<DataT>(instanceName, visualData, info);
    }

    void ProviderComponentPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {
        ComponentPlugin::postCreatePropertyDefinitions(properties);
    }

    void ProviderComponentPlugin::componentPropertiesUpdated(const std::set<std::string>& changedProperties)
    {
        ComponentPlugin::componentPropertiesUpdated(changedProperties);
    }

    void ProviderComponentPlugin::preOnInitComponent()
    {
    }

    void ProviderComponentPlugin::postOnInitComponent()
    {
        ManagedIceObjectPlugin::postOnInitComponent();
    }

    void ProviderComponentPlugin::preOnConnectComponent()
    {
        ManagedIceObjectPlugin::preOnConnectComponent();
    }

    void ProviderComponentPlugin::postOnConnectComponent()
    {
        ManagedIceObjectPlugin::postOnConnectComponent();
    }

    void ProviderComponentPlugin::preOnDisconnectComponent()
    {
        ManagedIceObjectPlugin::preOnDisconnectComponent();
    }

    void ProviderComponentPlugin::postOnDisconnectComponent()
    {
        ManagedIceObjectPlugin::postOnDisconnectComponent();
    }

    void ProviderComponentPlugin::preOnExitComponent()
    {
        ARMARX_TRACE;
        blobProviderMap.clear();
    }

    void ProviderComponentPlugin::postOnExitComponent()
    {
        ManagedIceObjectPlugin::postOnExitComponent();
    }

    void ProviderComponentPlugin::addProvider(const std::string& providerName,
                                              const arondto::ProviderMetaInfo& info)
    {
        ARMARX_TRACE;
        auto blobProvider = IceInternal::Handle<CameraServer>(
                new CameraServer(providerName, info));
        parent().getArmarXManager()->addObject(blobProvider);
        blobProviderMap.emplace(providerName, std::make_pair(blobProvider, info));
    }

    void ProviderComponentPlugin::addVisualData(const std::string& providerName, const std::string& instanceName,
                                                const arondto::DataInstanceDescription type)
    {
        ARMARX_TRACE;
        blobProviderMap.at(providerName).first->addVisualData(instanceName, type);
    }

    void ProviderComponentPlugin::provide(const cv::Mat& image, const std::string& providerName,
                                          const std::string& instanceName,
                                          const std::optional<armarx::DateTime>& provisionTimestamp)
    {
        ARMARX_TRACE;
        vision::core::arondto::DataInstanceMetaInfo info;
        // TODO: maybe capture timestamp would be good
        info.provisionTimestamp = provisionTimestamp.value();
        info.size.width = image.cols;
        info.size.height = image.rows;
        info.size.bytesPerElement = image.elemSize();
        info.compressionQuality = vision::core::arondto::CompressionQuality::Low;
        info.type = vision::core::arondto::VisualDataType::Image;
        provide(image, info, providerName, instanceName);
    }

    template <typename PointT>
    void ProviderComponentPlugin::provide(const pcl::PointCloud<PointT>& pointCloud, const std::string& providerName,
                                          const std::string& instanceName)
    {
        ARMARX_TRACE;
        vision::core::arondto::DataInstanceMetaInfo info;
        // TODO: maybe capture timestamp would be good
        info.provisionTimestamp = armarx::core::time::DateTime::Now();
        info.size.width = pointCloud.width;
        info.size.height = pointCloud.height;
        info.size.bytesPerElement = sizeof(PointT);
        info.type = vision::core::traits::PointCloud<pcl::PointCloud<PointT>>::enum_value;
        provide(pointCloud, info, providerName, instanceName);
    }

    FOR_EACH_POINTCLOUD(EXPLICIT_PC_PROVIDE_INSTANTIATION);
}