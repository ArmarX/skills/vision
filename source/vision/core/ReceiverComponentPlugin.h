/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       22.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vision/core/CameraClient.h>
#include <ArmarXCore/core/ComponentPlugin.h>
#include <ArmarXCore/core/time.h>

namespace armarx::vision::core
{
    class ReceiverComponentPlugin : public ComponentPlugin
    {
    public:
        template <VisualDataT T> using VisualDataType = typename traits::VisualData<T>::type;

        ReceiverComponentPlugin(ManagedIceObject& parent, const std::string& prefix);

        void addConsumer(const std::string& providerName);

        template <typename DataT>
        DataT getVisualData(const std::string& providerName, const std::string& instanceName,
                             const std::optional<armarx::DateTime>& newerThan) const;

        [[nodiscard]] arondto::DataInstanceMetaInfo
        getMetaInfo(const std::string& providerName, const std::string& instanceName,
                    const std::optional<armarx::DateTime>& newerThan) const;

        template <typename DataT>
        std::pair<DataT, arondto::DataInstanceMetaInfo>
        getVisualDataAndMetaInfo(const std::string& providerName, const std::string& instanceName,
                                  const std::optional<armarx::DateTime>& newerThan) const;

        template <typename ...DataTs>
        [[nodiscard]] std::tuple<DataWithMetaInfo<DataTs>...>
        getVisualDataAndMetaInfo(const std::string& providerName, const std::vector<std::string>& instanceNames,
                                   const std::optional<armarx::DateTime>& newerThan) const;

        cv::Mat getImage(const std::string& providerName = "", const std::string& instanceName = "",
                         const std::optional<armarx::DateTime>& newerThan = armarx::DateTime::Now()) const;

        std::pair<cv::Mat, arondto::DataInstanceMetaInfo>
        getImageAndMetaInfo(const std::string& providerName = "", const std::string& instanceName = "",
                            const std::optional<armarx::DateTime>& newerThan = armarx::DateTime::Now()) const;

    protected:
        void preOnConnectComponent() override;

    private:
        [[nodiscard]] CameraClient& getCameraClient(const std::string& providerName) const;

        std::map<std::string, IceInternal::Handle<CameraClient>> blobConsumerMap;
    };

    extern template std::tuple<DataWithMetaInfo<cv::Mat>, DataWithMetaInfo<cv::Mat>>
    ReceiverComponentPlugin::getVisualDataAndMetaInfo<cv::Mat, cv::Mat>(const std::string&,
                                                                          const std::vector<std::string>&,
                                                                          const std::optional<armarx::DateTime>&) const;
}
