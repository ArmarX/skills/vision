/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       16.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/services/sharedmemory/HardwareIdentifierProvider.h>

#include <future>
#include <vision/core/Interface.h>
#include <vision/core/Traits.h>
#include <vision/core/shared_memory/SharedMemoryProvider.h>
#include "VisualDataManager.h"

#define EXPLICIT_PC_PROVIDE_INSTANTIATION_DECLARATION(r, data, elem) \
extern template                                                      \
void CameraServer::update<elem>(const std::string&, const elem&, \
const arondto::DataInstanceMetaInfo&);

namespace armarx::vision::core
{
    class CameraServer : virtual public itfc::BlobProviderInterface, virtual public HardwareIdentifierProvider, virtual public ManagedIceObject
    {

    public:
        CameraServer(const std::string& providerName, const arondto::ProviderMetaInfo& metaInfo);

        std::string getDefaultName() const override;

        void onInitComponent() override;

        void onConnectComponent() override;

        void addSharedMemoryConsumer(const Ice::Current& current) override;

        void removeSharedMemoryConsumer(const Ice::Current& current) override;

        void addIceConsumer(const Ice::Current& current) override;

        void removeIceConsumer(const Ice::Current& current) override;

        void getBlobNewerThan_async(const itfc::AMD_BlobProviderInterface_getBlobNewerThanPtr& ptr,
                                    const std::string& instanceName, const armarx::core::time::dto::DateTime& dateTime,
                                    ::Ice::Long waitForMS, const Ice::Current& current) override;

        void getBlobImmediately_async(const itfc::AMD_BlobProviderInterface_getBlobImmediatelyPtr& ptr,
                                      const std::string& instanceName, const Ice::Current& current) override;

        // TODO: use perfect forwarding
        template <typename DataT>
        void update(const std::string& instanceName, const DataT& visualData,
                    const arondto::DataInstanceMetaInfo& info);

        void addVisualData(const std::string& instanceName, const arondto::DataInstanceDescription& type);
    private:
        void checkAndRemoveAMDFutures();


        std::atomic<unsigned int> numberOfIceConsumers;
        std::atomic<unsigned int> numberOfSharedMemoryConsumers;
        std::unique_ptr<VisualDataDataManager> dataManager;
        std::mutex newerThanMutex;
        std::condition_variable newerThanCondition;
        std::vector<std::future<void>> amdFutures;
        std::string memoryName;
    };

    FOR_EACH_TYPE(EXPLICIT_PC_PROVIDE_INSTANTIATION_DECLARATION);
}

#undef EXPLICIT_PC_PROVIDE_INSTANTIATION_DECLARATION