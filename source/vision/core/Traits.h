/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       18.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <variant>
#include <vision/core/Macros.h>
#include <vision/core/Meta.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#define TYPE_STRUCT_SPECIALIZATION(r, data, elem) \
template <> struct VisualData<ENUM_VALUE(elem)>{using type = ENUM_TYPE(elem);};

#define POINTCLOUD_TYPE_TO_ENUM_VALUE(r, data, elem) \
template <> struct PointCloud<ENUM_TYPE(elem)>{      \
static constexpr VisualDataT enum_value = ENUM_VALUE(elem); \
using point_type = typename ENUM_TYPE(elem)::PointType; \
};

namespace armarx::vision::core
{
    using VisualDataT = arondto::VisualDataType::ImplEnum;
    namespace traits
    {
        template <VisualDataT>
        struct VisualData;
        template <typename T>
        struct PointCloud;
        FOR_EACH_ENUM_PAIR(TYPE_STRUCT_SPECIALIZATION);
        FOR_EACH_POINTCLOUD(POINTCLOUD_TYPE_TO_ENUM_VALUE);
    }

}

#undef POINTCLOUD_TYPE_TO_ENUM_VALUE
#undef TYPE_STRUCT_SPECIALIZATION