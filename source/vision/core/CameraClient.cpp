/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       16.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CameraClient.h"

#include <ArmarXCore/core/services/sharedmemory/HardwareId.h>
#include <ArmarXCore/core/services/sharedmemory/exceptions/SharedMemoryExceptions.h>
#include <ArmarXCore/core/time/ice_conversions.h>
#include <opencv2/core/mat.hpp>
#include <vision/core/communication/Decoding.h>
#include <vision/core/shared_memory/SharedMemoryQueryHelper.h>

namespace armarx::vision::core
{
    CameraClient::CameraClient(armarx::ManagedIceObject* object, const std::string& providerName) :
            memoryName(providerName + ".SharedMemoryProvider"),
            blobProviderName(providerName + ".IceBlobProvider")
    {
        ARMARX_TRACE;
        consumerName = object->getName() + "_" + providerName + ".IceBlobConsumer";
        object->usingProxy(blobProviderName);
    }

    std::string CameraClient::getHardwareId()
    {
        ARMARX_TRACE;
        return armarx::tools::getHardwareId();
    }

    CameraClient::~CameraClient()
    {
        ARMARX_TRACE;
        try
        {
            if (blobProvider)
            {
                switch (transferMode)
                {
                    case TransferMode::Ice:
                        blobProvider->removeIceConsumer();
                        break;
                    case TransferMode::SharedMemory:
                        blobProvider->removeSharedMemoryConsumer();
                        break;
                }
            }
        } catch (Ice::ObjectAdapterDeactivatedException&)
        {
            ARMARX_WARNING << "Object Adapter for the blob provider was already deactivated";
        }
    }

    template <typename DataT>
    DataT CameraClient::getVisualData(const std::string& instanceName,
                                       const std::optional<armarx::DateTime>& newerThan) const
    {
        ARMARX_TRACE;
        return getVisualDataAndMetaInfo<DataT>(instanceName, newerThan).first;
    }

    arondto::DataInstanceMetaInfo
    CameraClient::getMetaInfo(const std::string& instanceName, const std::optional<armarx::DateTime>& newerThan) const
    {
        // TODO: unite this with getVisualData
        ARMARX_TRACE;
        auto now = armarx::core::time::DateTime::Now();
        switch (transferMode)
        {
            case TransferMode::Ice:
            {
                ARMARX_TRACE;
                auto blob = itfc::BlobWithMetaInfo();
                ARMARX_CHECK_NOT_NULL(blobProvider);
                if (newerThan.has_value())
                {
                    armarx::core::time::dto::DateTime time;
                    toIce(time, now);
                    blob = blobProvider->getBlobNewerThan(instanceName, time, 100);
                } else
                {
                    blob = blobProvider->getBlobImmediately(instanceName);
                }
                arondto::DataInstanceMetaInfo info;
                info.fromAron(blob.metaInfo);
                return info;
            }
            case TransferMode::SharedMemory:
            {
                ARMARX_TRACE;
                ARMARX_CHECK_NOT_NULL(sharedMemoryConsumer);
                if (newerThan.has_value())
                {
                    const auto& buffer = sharedMemoryConsumer->getMemoryBuffer(instanceName)
                                                             .newerThan(newerThan.value(),
                                                                        armarx::core::time::Duration::Seconds(2));
                    return communication::shared_memory::MetaInfoHandle(buffer.info).decode();
                } else
                {
                    const auto& buffer = sharedMemoryConsumer->getMemoryBuffer(instanceName).immediately();
                    return communication::shared_memory::MetaInfoHandle(buffer.info).decode();
                }
            }
            default:
                throw armarx::LocalException("Unhandled Enum value");
        }
    }

    std::string CameraClient::getDefaultName() const
    {
        return consumerName;
    }

    void CameraClient::onInitComponent()
    {

    }

    void CameraClient::onConnectComponent()
    {
        ARMARX_TRACE;
        // Ice Proxy
        try
        {
            blobProvider = getProxy<itfc::BlobProviderInterfacePrx>(blobProviderName, true);
        } catch (std::exception& e)
        {
            std::stringstream message;
            message
                    << "Error retrieving shared memory proxy. An exception occured when calling object->getProxy<SharedMemoryProviderInterfacePrx>():"
                    << "\nOriginal exception: " << e.what();
            throw exceptions::local::SharedMemoryConnectionFailure(memoryName, message.str());

        } catch (...)
        {
            throw exceptions::local::SharedMemoryConnectionFailure(memoryName,
                                                                   "Error retrieving shared memory proxy. An exception occured when calling object->getProxy<SharedMemoryProviderInterfacePrx>().");
        }

        if (!blobProvider)
        {
            throw exceptions::local::SharedMemoryConnectionFailure(memoryName,
                                                                   "Error retrieving shared memory proxy: The returned memory proxy is NULL.");
        }
        // Shared Memory
        if (getHardwareId() == "00:00:00:00:00:00")
        {
            transferMode = TransferMode::Ice;
            blobProvider->addIceConsumer();
            ARMARX_WARNING_S << "Unable to determine hardware id. Using ice memory transfer as fallback.";
        } else if (blobProvider->getHardwareId() == getHardwareId())
        {
            try
            {
                sharedMemoryConsumer = std::make_unique<SharedMemoryConsumerT>(memoryName);
            } catch (std::exception& e)
            {
                transferMode = TransferMode::Ice;
                blobProvider->addIceConsumer();
                ARMARX_WARNING << "Initialization of shared memory '" << memoryName
                               << "' failed - failing back to Ice memory transfer - reason: " << e.what();
                return;
            }
            blobProvider->addSharedMemoryConsumer();
            transferMode = TransferMode::SharedMemory;
            ARMARX_INFO << "Using shared memory transfer" << flush;

        } else
        {
            transferMode = TransferMode::Ice;
            blobProvider->addIceConsumer();
            ARMARX_INFO << "Using Ice memory transfer" << flush;
        }
    }

    template <typename... DataTs>
    std::tuple<DataWithMetaInfo<DataTs>...>
    CameraClient::getVisualDataAndMetaInfo(const std::vector<std::string>& instanceNames,
                                             const std::optional<armarx::DateTime>& newerThan) const
    {
        std::vector<std::string> localInstanceNames = instanceNames;
        return {getVisualDataAndMetaInfo<DataTs...>(localInstanceNames, newerThan)};
    }

    template <typename T1, typename... Ts>
    std::tuple<DataWithMetaInfo<T1>, DataWithMetaInfo<Ts>...>
    CameraClient::getVisualDataAndMetaInfo(std::vector<std::string>& instanceNames,
                                             const std::optional<armarx::DateTime>& newerThan) const
    {
        auto vector_pop_back = [&instanceNames]() -> std::string
        {
            std::string back_elem = instanceNames.back();
            instanceNames.pop_back();
            return back_elem;
        };
        return std::tuple_cat(
                std::tuple<DataWithMetaInfo<T1>>(getVisualDataAndMetaInfo<T1>(vector_pop_back(), newerThan)),
                getVisualDataAndMetaInfo<Ts>(instanceNames, newerThan)...);
    }

    template <typename DataT>
    std::pair<DataT, arondto::DataInstanceMetaInfo>
    CameraClient::getVisualDataAndMetaInfo(const std::string& instanceName,
                                            const std::optional<DateTime>& newerThan) const
    {
        switch (transferMode)
        {
            case TransferMode::Ice:
            {
                ARMARX_TRACE;
                auto blob = itfc::BlobWithMetaInfo();
                ARMARX_CHECK_NOT_NULL(blobProvider);
                if (newerThan.has_value())
                {
                    armarx::core::time::dto::DateTime now;
                    toIce(now, newerThan.value());
                    blob = blobProvider->getBlobNewerThan(instanceName, now, 2000);
                } else
                {
                    blob = blobProvider->getBlobImmediately(instanceName);
                }
                return communication::decode<DataT>(blob);
            }
            case TransferMode::SharedMemory:
            {
                ARMARX_TRACE;
                ARMARX_CHECK_NOT_NULL(sharedMemoryConsumer);
                const auto& query = sharedMemoryConsumer->getMemoryBuffer(instanceName);
                if (newerThan.has_value())
                {
                    const auto& buffer = query.newerThan(newerThan.value(), armarx::core::time::Duration::Seconds(2));
                    return communication::decode<DataT>(buffer);
                } else
                {
                    const auto& buffer = query.immediately();
                    return communication::decode<DataT>(buffer);
                }
            }
            default:
                throw armarx::LocalException("Unhandled Enum value");
        }
    }

    template cv::Mat
    CameraClient::getVisualData<cv::Mat>(const std::string&, const std::optional<armarx::DateTime>&) const;

    template std::tuple<DataWithMetaInfo<cv::Mat>, DataWithMetaInfo<cv::Mat>>
    CameraClient::getVisualDataAndMetaInfo(const std::vector<std::string>&,
                                             const std::optional<armarx::DateTime>&) const;
}