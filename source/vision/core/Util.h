/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       28.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <opencv2/core.hpp>
#include <pcl/common/generate.h>


namespace armarx::vision::core::util
{
    inline bool imagesAreEqual(const cv::Mat& image1, const cv::Mat& image2)
    {
        if ((image1.rows != image2.rows) || (image1.cols != image2.cols))
            return false;
        if (image1.type() != image2.type())
            return false;
        cv::Scalar s = cv::sum(image1 - image2);
        return (s[0] == 0) && (s[1] == 0) && (s[2] == 0);
    }

    inline cv::Mat generateRandomImage(std::size_t width, std::size_t height, int type)
    {
        auto image = cv::Mat(height, width, type);
        double mean = 0.0;
        double stddev = 500.0 / 3.0; // 99.7% of values will be inside [-500, +500] interval
        cv::randn(image, cv::Scalar(mean), cv::Scalar(stddev));
        return image;
    }

    template <typename PointT> pcl::PointCloud<PointT> generateRandomPointCloud(std::size_t width, std::size_t height)
    {
        pcl::PointCloud<PointT> cloud(width, height);
        for (auto& point : cloud.points)
        {
            point.x = std::rand();
            point.y = std::rand();
            point.z = std::rand();
        }
        return cloud;
    }

    template <typename PointT> bool pointCloudsAreEqual(const pcl::PointCloud<PointT>& cloud1, const pcl::PointCloud<PointT>& cloud2)
    {
        if (cloud1.size() != cloud2.size() or cloud1.height != cloud2.height or cloud1.width != cloud2.width) return false;
        bool equal = true;
        for (std::size_t i = 0; i < cloud1.size(); i++)
        {
            auto a = cloud1.at(i);
            auto b = cloud2.at(i);
            equal &= a.x == b.x;
            equal &= a.y == b.y;
            equal &= a.z == b.z;
            if (!equal)
            {
                std::cout << "False at " << i << "\n";
                return false;
            }
        }
        return true;
    }
}
