/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       16.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/core/SharedMemory.ice>
#include <ArmarXCore/interface/core/time.ice>
#include <RobotAPI/interface/aron.ice>

module armarx
{

    module vision
    {
        module core
        {
            module itfc
            {
                struct BlobWithMetaInfo
                {
                    armarx::Blob blob;
                    armarx::aron::data::dto::Dict metaInfo;
                }

                dictionary<string, BlobWithMetaInfo> BlobsWithMetaInfo;
                interface BlobProviderInterface extends armarx::HardwareIdentifierProviderInterface
                {
                    void addSharedMemoryConsumer();
                    void removeSharedMemoryConsumer();
                    void addIceConsumer();
                    void removeIceConsumer();
                    // ["amd"] BlobsWithMetaInfo getBlobs();
                    // ["amd"] BlobsWithMetaInfo getBlobsNewerThan(armarx::core::time::dto::DateTime time, long waitForMS);
                    ["amd"] BlobWithMetaInfo getBlobNewerThan(string instanceName, armarx::core::time::dto::DateTime time, long waitForMS);
                    ["amd"] BlobWithMetaInfo getBlobImmediately(string instanceName);
                }
            }
        }
    }
}