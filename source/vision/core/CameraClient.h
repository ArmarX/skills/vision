/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       16.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/ManagedIceObject.h>

#include <future>
#include <vision/core/Traits.h>
#include <vision/core/Interface.h>
#include <vision/core/shared_memory/SharedMemoryConsumer.h>
#include <vision/core/aron/DataInstanceMetaInfo.aron.generated.h>


namespace armarx::vision::core
{
    class CameraClient : virtual public ManagedIceObject
    {
        using SharedMemoryConsumerT = shared_memory::SharedMemoryConsumer;
    public:
        explicit CameraClient(armarx::ManagedIceObject* object, const std::string& providerName);

        std::string getDefaultName() const override;

        void onInitComponent() override;

        void onConnectComponent() override;

        template <typename DataT>
        DataT getVisualData(const std::string& instanceName, const std::optional<armarx::DateTime>& newerThan) const;

        arondto::DataInstanceMetaInfo
        getMetaInfo(const std::string& instanceName, const std::optional<armarx::DateTime>& newerThan) const;

        template <typename DataT>
        std::pair<DataT, arondto::DataInstanceMetaInfo>
        getVisualDataAndMetaInfo(const std::string& instanceName, const std::optional<DateTime>& newerThan) const;

        template <typename ...DataTs>
        std::tuple<DataWithMetaInfo<DataTs>...>
        getVisualDataAndMetaInfo(const std::vector<std::string>& instanceNames,
                                   const std::optional<armarx::DateTime>& newerThan) const;

        template <typename T1, typename ...Ts>
        std::tuple<DataWithMetaInfo<T1>, DataWithMetaInfo<Ts>...>
        getVisualDataAndMetaInfo(std::vector<std::string>& instanceNames,
                                   const std::optional<armarx::DateTime>& newerThan) const;

        ~CameraClient();

    private:
        static std::string getHardwareId();

        std::unique_ptr<SharedMemoryConsumerT> sharedMemoryConsumer;
        std::string memoryName;
        std::string consumerName;
        std::string blobProviderName;
        itfc::BlobProviderInterfacePrx blobProvider;

        enum class TransferMode
        {
            SharedMemory, Ice
        };

        TransferMode transferMode{TransferMode::SharedMemory};

    };

    extern template cv::Mat
    CameraClient::getVisualData<cv::Mat>(const std::string&, const std::optional<armarx::DateTime>&) const;

    extern template std::tuple<DataWithMetaInfo<cv::Mat>, DataWithMetaInfo<cv::Mat>>
    CameraClient::getVisualDataAndMetaInfo(const std::vector<std::string>&,
                                             const std::optional<armarx::DateTime>&) const;
}
