/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       20.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

namespace cv
{
    class Mat;
}

namespace pcl
{
    template <typename T>
    class PointCloud;

    class PointXYZ;

    class PointXYZI;

    class PointXYZL;

    class PointXYZLNormal;

    class PointXYZRGB;

    class PointXYZRGBA;

    class PointXYZRGBL;

    class PointXYZRGBNormal;

    class PrincipalCurvatures;
}

namespace armarx::vision
{
    namespace core
    {
        namespace arondto
        {
            class VisualDataType;

            class CompressionQuality;

            class DataInstanceMetaInfo;

            class DataInstanceDescription;

            class ProviderMetaInfo;

            class IntrinsicMatrix;

            class ExtrinsicMatrix;

            class DistortionParameters;

            class CameraCalibration;
        }

        class CameraServer;

        class CameraClient;

        class BlobWithMetaInfo;

        class VisualDataManager;

        class ProviderComponentPlugin;

        class ReceiverComponentPlugin;

    }
    using ProviderMetaInfo = core::arondto::ProviderMetaInfo;
    using DataInstanceMetaInfo = core::arondto::DataInstanceMetaInfo;
    using DataInstanceDescription = core::arondto::DataInstanceDescription;
    using VisualDataType = core::arondto::VisualDataType;
    using CameraCalibration = core::arondto::CameraCalibration;
    using ProviderComponentPlugin = core::ProviderComponentPlugin;
    using ReceiverComponentPlugin = core::ReceiverComponentPlugin;
}