/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       18.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vision/core/ProviderComponentPlugin.h>
#include <vision/core/ReceiverComponentPlugin.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

namespace armarx::vision::components::image_provider_test
{
    class ComponentPropertyDefinitions :
            public armarx::ComponentPropertyDefinitions
    {
    public:
        ComponentPropertyDefinitions(std::string prefix);
    };

    class CameraProviderComponent :
            virtual public armarx::Component
    {
    public:
        CameraProviderComponent();
        std::string getDefaultName() const override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
    private:

        void runningTask();

        armarx::SimplePeriodicTask<std::function<void(void)>>::pointer_type running_task_;
        std::experimental::observer_ptr<core::ReceiverComponentPlugin> receiver_plugin_;
        std::experimental::observer_ptr<core::ProviderComponentPlugin> provider_plugin_;

    };
}

