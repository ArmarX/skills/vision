/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       24.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ImageConsumerComponent.h"

#include <pcl/common/generate.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <opencv2/core.hpp>

namespace armarx::vision::components::image_consumer_test
{

    ARMARX_DECOUPLED_REGISTER_COMPONENT(ImageConsumerComponent);

    std::string ImageConsumerComponent::getDefaultName() const
    {
        return "ImageConsumerTest";
    }

    void ImageConsumerComponent::onInitComponent()
    {
        receiver_plugin_->addConsumer("Test");
    }

    void ImageConsumerComponent::onConnectComponent()
    {
        running_task_ = std::make_unique<armarx::SimplePeriodicTask<>>([this]{
            runningTask();
        }, 100);
        running_task_->start();
    }

    void ImageConsumerComponent::onDisconnectComponent()
    {
        ManagedIceObject::onDisconnectComponent();
    }

    void ImageConsumerComponent::onExitComponent()
    {
        ManagedIceObject::onExitComponent();
    }

    armarx::PropertyDefinitionsPtr ImageConsumerComponent::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());
        return defs;
    }

    void ImageConsumerComponent::runningTask()
    {
        ARMARX_INFO << "Waiting for image";
//        auto metainfo = getMetaInfo("Test");
        auto image2 = receiver_plugin_->getImage("Test", "");
        ARMARX_INFO << VAROUT(image2.type()) << VAROUT(image2.rows) << VAROUT(image2.cols);
    }

    ImageConsumerComponent::ImageConsumerComponent()
    {
        addPlugin(receiver_plugin_);
    }

    ComponentPropertyDefinitions::ComponentPropertyDefinitions(std::string prefix) :
            armarx::ComponentPropertyDefinitions(std::move(prefix)) {}

}