/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_vision
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       18.09.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CameraProviderComponent.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <vision/core/Util.h>

namespace armarx::vision::components::image_provider_test
{
    ARMARX_DECOUPLED_REGISTER_COMPONENT(CameraProviderComponent);

    std::string CameraProviderComponent::getDefaultName() const
    {
        return "ImageProviderTest";
    }

    void CameraProviderComponent::onInitComponent()
    {
        vision::core::arondto::ProviderMetaInfo info;
        vision::core::arondto::DataInstanceDescription img_desc;
        img_desc.type = core::arondto::VisualDataType::Image;
        vision::core::arondto::DataInstanceDescription pc_desc;
        pc_desc.type = core::arondto::VisualDataType::PointCloudXYZ;
        info.visualData.insert(std::make_pair("Images", img_desc));
        TIMING_START(addProviderT);
        provider_plugin_->addProvider("Test", info);
        TIMING_END(addProviderT);
        TIMING_START(addConsumerT);
        receiver_plugin_->addConsumer("Test");
        TIMING_END(addConsumerT);
        provider_plugin_->addVisualData("Test", "PointClouds", pc_desc);
        provider_plugin_->addVisualData("Test", "StereoLeft", img_desc);
        provider_plugin_->addVisualData("Test", "StereoRight", img_desc);
    }

    void CameraProviderComponent::onConnectComponent()
    {
        running_task_ = new armarx::SimplePeriodicTask<>([this]
                                                         {
                                                             runningTask();
                                                         }, 100);
        running_task_->start();
        //        this->getArmarXManager()->asyncShutdown(10000);
    }

    void CameraProviderComponent::onDisconnectComponent()
    {
        running_task_->stop();
    }

    void CameraProviderComponent::onExitComponent()
    {
    }

    armarx::PropertyDefinitionsPtr CameraProviderComponent::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());
        return defs;
    }

    void CameraProviderComponent::runningTask()
    {
        auto image1 = core::util::generateRandomImage(10, 20, CV_8UC3);
        auto image2 = core::util::generateRandomImage(10, 20, CV_8UC3);
        auto image3 = core::util::generateRandomImage(10, 20, CV_8UC3);
        //        auto image = cv::Mat(10, 20, CV_16UC3, cv::Scalar(1, 3, 2));
        auto pc = core::util::generateRandomPointCloud<pcl::PointXYZRGB>(1000, 1000);
        auto future = std::async(std::launch::async, [this, &image1]()
        {
            ARMARX_INFO << "Waiting for image";
            auto query1 = receiver_plugin_->getImage("Test", "Images");
            ARMARX_CHECK(core::util::imagesAreEqual(image1, query1));
        });
        auto future2 = std::async(std::launch::async, [this, &image2, &image3]()
        {
            auto stereoImages = receiver_plugin_
                    ->getVisualDataAndMetaInfo<cv::Mat, cv::Mat>("Test", {"StereoLeft", "StereoRight"},
                                                                   armarx::DateTime::Now());
            ARMARX_CHECK(core::util::imagesAreEqual(image2, std::get<0>(stereoImages).first));
            ARMARX_CHECK(core::util::imagesAreEqual(image3, std::get<1>(stereoImages).first));
        });
        ARMARX_INFO << "Providing Image";
        sleep(1);
        TIMING_START(provideImage);
        auto now = armarx::DateTime::Now();
        provider_plugin_->provide(image1, "Test", "Images", now);
        provider_plugin_->provide(image2, "Test", "StereoLeft", now);
        provider_plugin_->provide(image3, "Test", "StereoRight", now);
        future.get();
        future2.get();
        TIMING_END(provideImage);
        TIMING_START(providePC);
        provider_plugin_->provide(pc, "Test", "PointClouds");
        TIMING_END(providePC);
    }

    CameraProviderComponent::CameraProviderComponent()
    {
        addPlugin(provider_plugin_);
        addPlugin(receiver_plugin_);
    }

    ComponentPropertyDefinitions::ComponentPropertyDefinitions(std::string prefix) :
            armarx::ComponentPropertyDefinitions(std::move(prefix))
    {
    }
}


